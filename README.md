# Sum Two Program
Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

 

Examples:
```sh
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
```
```sh
Input: nums = [3,2,4], target = 6
Output: [1,2]
```
```sh
Input: nums = [3,3], target = 6
Output: [0,1]
```

# Approach
Array is truncated every iteration to not check the same case
# Complexity
- Time complexity:
$$O(n^2/2)$$

- Space complexity:
$$O(1)$$

# Code
```
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */

var twoSum = function (nums, target) {

    const numsLength = nums.length; 
    const numsCopy = Array.from(nums);
    let numsCpLength = numsCopy.length; 

    for (let i = numsLength - numsCpLength; i < numsLength; i++) {
        for (let j = numsLength - numsCpLength; j < numsLength; j++) {

            if ((i != j) && (nums[i] + nums[j] === target)) {
                return [i, j];
            }
        }
        numsCopy.shift();
        numsCpLength = numsCopy.length;
    }
};
```